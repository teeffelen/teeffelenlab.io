---
layout: post
title: "Ineffective Coding Habits"
description: "Seven habits you should avoid"
tags: []
---

In this talk, Kevlin Henney goes over seven rather ineffective coding habits of
most programmers:

1. **Noisy code**
    -- Stop adding additional noise to your code (6:53)

2. **Unsustainable spacing**
    -- Make your code readable and stable under refactoring (29:18)

3. **Lego naming**
    -- Don't dilute the meaning of names in your code (50:30)

4. **Underabstraction**
    -- Make sure the purpose of your code is clear (1:06:56)

5. **Unencapsulated state**
    -- Keep your code properly private (1:21:20)

6. **Getters and setters**
    -- Get rid of the get and set in your code (1:27:30)

7. **Uncohesive tests**
    -- A test case should correspond to a single case (1:33:42)

{% include youtube.html id="oyyFKHpzL0Q" %}

> **Note:** _The original talk has been removed from YouTube, the video shown
> above is a similar recording I found._
