---
layout: default
description: "Software Engineer & Tech Enthusiast"
sitemap: true
---

Read [more about me][about], or checkout one of the
[{{ site.categories.blog.size }} blog posts][blog] and
[{{ site.categories.tips.size }} tips][tips]!

[about]: {% link about.md %}
[blog]: {% link blog/index.md %}
[tips]: {% link tips/index.md %}

## Recent blogs

{% for post in site.categories.blog limit:10 %}
1. [**{{ post.title }}**]({{ post.url }}){% if post.description %} -- {{ post.description }}{% endif %}
{% endfor %}

[View all blog posts &rarr;]({% link blog/index.md %})

## Recent tips

{% for post in site.categories.tips limit:10 %}
1. [**{{ post.title }}**]({{ post.url }}){% if post.description %} -- {{ post.description }}{% endif %}
{% endfor %}

[View all tips &rarr;]({% link tips/index.md %})
