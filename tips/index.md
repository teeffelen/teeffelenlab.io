---
layout: default
title: "Tips"
description: "Overview"
sitemap: true
---

<style>
ul {
    list-style: none;
    padding-left: 2rem;
}
</style>

{% for post in site.categories.tips -%}
{% capture cur_year %}{{ post.date | date: "%Y" }}{% endcapture -%}

{% if cur_year != last_year -%}

### {{ cur_year }}

{% assign last_year = cur_year -%}
{% endif -%}

- {{ post.date | date: "%b %-d" }} -- **[{{ post.title }}]({{ post.url }})**

{% endfor %}
