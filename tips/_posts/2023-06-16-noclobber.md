---
layout: post
title: "No Clobber"
description: "Never overwrite files by accident"
tags: [linux, tips]
---

Ever unintentionally [clobbered] a file when redirecting some output to it?

```bash
$ cat some_file
foo bar

# Unintentionally overwrite 'some_file'
$ echo "potato" > some_file

$ cat some_file
potato
```

This can happen whenever the output file already exists. Or when you
accidentally write to the file with `>` instead of appending to it using `>>`.

Most Linux shells actually have an option to prevent you from unintentionally
overwriting files. It is generally called "no clobber" and you can enable it
as follows:

```bash
# On Bash ~/.bashrc
set -o noclobber

# On Zsh ~/.zshrc
setopt NO_CLOBBER
```

This will make sure that the filename does not exist before trying to redirect
data into it:

```bash
$ cat some_file
foo bar

# Unintentionally overwrite 'some_file'
$ echo "potato" > some_file
zsh: file exists: some_file

# 'some_file' remains untouched
$ cat some_file
foo bar
```

But what if we **intentionally** want to overwrite a file?
In that case, we can tell bash to ignore the clobber by using `>|` instead:

```bash
$ echo "potato" >| some_file

$ cat some_file
potato
```

[clobbered]: https://en.wikipedia.org/wiki/Clobbering
