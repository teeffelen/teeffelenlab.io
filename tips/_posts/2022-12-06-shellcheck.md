---
layout: post
title: "ShellCheck"
description: "Avoid the \"But it works on my machine\""
tags: [linux, tips]
---

Maybe you've encountered the famous _"But it works on my machine"_ before,
using a linter such as [ShellCheck] can (mostly) prevent this from
happening, by warning you of potential issues in your script.

After installing it, you can check your script by parsing it to [ShellCheck]:

```bash
shellcheck some-script.sh
```

If you're using Microsoft's Visual Studio Code, you can also install the
[ShellCheck Extension] to integrate ShellCheck directly in your editor 😎

[ShellCheck]: https://github.com/koalaman/shellcheck
[ShellCheck Extension]: https://marketplace.visualstudio.com/items?itemName=timonwong.shellcheck
